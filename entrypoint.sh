#!/bin/bash
set -e

export PGPASSWORD=$TTRSS_DATABASE_PASSWORD

until psql -h "$TTRSS_DATABASE_HOST" -p "$TTRSS_DATABASE_PORT" -U "$TTRSS_DATABASE_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"

if [[ $(psql -h "$TTRSS_DATABASE_HOST" -p "$TTRSS_DATABASE_PORT" -U "$TTRSS_DATABASE_USER" -tAc "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'ttrss_feeds');") = 't' ]]; then
  echo "Database already initialized"
else
  echo "Import database dump"
  psql -h "$TTRSS_DATABASE_HOST" -p "$TTRSS_DATABASE_PORT" -U "$TTRSS_DATABASE_USER" "$TTRSS_DATABASE_NAME" < /var/www/html/schema/ttrss_schema_pgsql.sql
fi

echo "Start cron"
sed -i "s/%TTRSS_DATABASE_USER%/$TTRSS_DATABASE_USER/g" /var/www/html/feed-update.cron
sed -i "s/%TTRSS_DATABASE_PASSWORD%/$TTRSS_DATABASE_PASSWORD/g" /var/www/html/feed-update.cron
if [[ -v $REFRESH_INTERVAL_MINUTES ]];
then
  sed -i "s/%REFRESH_INTERVAL_MINUTES%/\/$REFRESH_INTERVAL_MINUTES/g" /var/www/html/feed-update.cron
else
  sed -i "s/%REFRESH_INTERVAL_MINUTES%/\/15/g" /var/www/html/feed-update.cron
fi
crontab /var/www/html/feed-update.cron
sudo cron

echo "Start server"
php -S 0.0.0.0:8080 -t /var/www/html
